import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'flip'
})
export class FlipPipe implements PipeTransform {

  transform(value: boolean): string {

    if(value){
      return 'Flip'
    }else{
      return 'Not flip'
    }
  }

}
