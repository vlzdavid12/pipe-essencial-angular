import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'upper'
})
export class UpperPipe implements PipeTransform {
  transform(value: string, params: boolean = false): string {
    if(params){
      return value.toUpperCase();
    }else{
      return value.toLowerCase();
    }

  }
}
