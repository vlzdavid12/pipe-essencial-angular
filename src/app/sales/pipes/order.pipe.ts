import { Pipe, PipeTransform } from '@angular/core';
import {Hero} from './../interface/hero.interface';

@Pipe({
  name: 'order'
})
export class OrderPipe implements PipeTransform {

  transform(heroes: Hero[], orderBy: string = ''): Hero[] {
    if(orderBy){
      switch (orderBy){
        case 'name':
          return heroes.sort((a,b) => (a[orderBy] > b[orderBy]) ? 1 : -1);
          break;
        case 'flip':
          return heroes.sort((a,b) => (a[orderBy] > b[orderBy]) ? -1 : 1);
          break;
        case  'color':
          return heroes.sort((a,b) => (a[orderBy] > b[orderBy]) ? 1 : -1);
          break;
        default:
          return heroes;
          break;
      }
    }else{
      return heroes;
    }
  }

}
