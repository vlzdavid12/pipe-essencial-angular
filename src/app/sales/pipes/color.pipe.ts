import { Pipe, PipeTransform } from '@angular/core';
import {Color} from '../interface/hero.interface';
@Pipe({
  name: 'color'
})
export class ColorPipe implements PipeTransform {

  transform(value: number): string {
    return Color[value];
  }

}
