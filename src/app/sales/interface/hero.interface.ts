export enum Color {
 black, blue , coffee, green,  yellow,   red
}


export interface Hero{
    name: string,
    flip: boolean,
    color: Color
}
