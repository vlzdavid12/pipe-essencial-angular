import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimeNgModule } from './../prime-ng/prime-ng.module';

import { NumberComponent } from './pages/number/number.component';
import { NotCommonsComponent } from './pages/not-commons/not-commons.component';
import { BasicComponent } from './pages/basic/basic.component';
import { OrderComponent } from './pages/order/order.component';

import { UpperPipe } from './pipes/upper.pipe';
import { FlipPipe } from './pipes/flip.pipe';
import { ColorPipe } from './pipes/color.pipe';
import { OrderPipe } from './pipes/order.pipe';

@NgModule({
  declarations: [
    NumberComponent,
    NotCommonsComponent,
    BasicComponent,
    OrderComponent,
    UpperPipe,
    FlipPipe,
    ColorPipe,
    OrderPipe
  ],
  exports: [
    NumberComponent,
    NotCommonsComponent,
    BasicComponent,
    OrderComponent
  ],
  imports: [
    CommonModule,
    PrimeNgModule
  ]
})
export class SalesModule { }
