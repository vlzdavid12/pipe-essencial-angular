import {Component, OnInit} from '@angular/core';
import {Hero, Color} from '../../interface/hero.interface';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styles: []
})
export class OrderComponent implements OnInit {

  optionFont: Boolean = false;
  orderOption: string = '';

  heroes: Hero[] = [{
    name: 'Superman',
    flip: true,
    color: Color.blue
  },
    {
      name: 'Batman',
      flip: false,
      color: Color.black
    },
    {
      name: 'Thor',
      flip: true,
      color: Color.coffee
    },
    {
      name: 'Capitan America',
      flip: false,
      color: Color.red
    },
    {
      name: 'Hulk',
      flip: false,
      color: Color.green
    },
    {
      name: 'Wonder Woman',
      flip: true,
      color: Color.yellow
    }
  ];


  constructor() {
  }

  ngOnInit(): void {
  }


  changeFont() {
    this.optionFont = !this.optionFont;
  }

  changeOrder(param: string){
    if(!param) return;
    this.orderOption = param;
  }



}
