import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styles: [
  ]
})
export class NumberComponent implements OnInit {
  saleNet: number = 23737.988;
  percent: number = 0.48;

  constructor() { }

  ngOnInit(): void {
  }

}
