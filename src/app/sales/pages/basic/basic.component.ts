import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styles: [
  ]
})
export class BasicComponent implements OnInit {

  nameLower: string = 'Lorem Ipsum is simply dummy';
  nameUpper: string = 'LOREM IPSUM IS SIMPLY DUMMY';
  nameTitle: string = 'lOREM IPSUM Is SImplY dUMMY';

  dateNow: Date = new Date();


  constructor() { }

  ngOnInit(): void {
  }

}
