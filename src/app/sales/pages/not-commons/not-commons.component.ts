import { Component } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-not-commons',
  templateUrl: './not-commons.component.html',
  styles: [
  ]
})
export class NotCommonsComponent  {

  //i18nSelect
  name: string = 'David';
  genero: string = 'male';

  invitationMap = {
    'male' :'invitarlo',
    'woman':'invitarla'
  }

  //i18nPlural
  clients: string[] = ['Maria', 'Pedro', 'Juan', 'Sebastina', 'David', 'Felipe'];
  clientsMap = {
    '=0': 'no tenemos clientes esperando.',
    '=1': 'tienes 1 cliente esperando.',
    '=2': 'tenemos 2 clientes esperando.',
    'other': 'tenemos # clientes esperando.',
  }

  //Key Value Pipe
  persons : {name: string, edad: number, direccion: string} =  {
    name: 'Rodrigo',
    edad: 35,
    direccion: 'Otta, Canda'
  }

  //Json Pipe

  hero : {name: string, value: boolean}[] = [
    {
      name: 'Superman',
      value: true
    },
    {
      name: 'Robin',
      value: false
    },
    {
      name: 'Aquaman',
      value: false
    }
  ]


  //Async Pipe

  miObservable = interval(2000);

  miPromise =  new Promise((resolve, reject)=>{
    setTimeout(()=>{
      resolve('Get success fully promise!')
    }, 2500)
  })



  changeNameClient(){
    this.name = 'Melisa';
    this.genero = 'woman';
  }

  eraseClient(){
    this.clients.pop();
  }

}
