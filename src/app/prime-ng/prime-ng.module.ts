import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MenubarModule} from 'primeng/menubar';
import {ButtonModule} from 'primeng/button';
import {CardModule} from 'primeng/card';
import {FieldsetModule} from 'primeng/fieldset';
import {ToolbarModule} from 'primeng/toolbar';
import {TableModule} from 'primeng/table';
import { TagModule } from 'primeng/tag';
import {SplitButtonModule} from 'primeng/splitbutton';

@NgModule({
  declarations: [],
  imports: [
    ButtonModule,
    CommonModule,
    CardModule,
    MenubarModule,
    FieldsetModule,
    SplitButtonModule,
    TableModule,
    TagModule,
    ToolbarModule
  ],
  exports: [
    CardModule,
    ButtonModule,
    MenubarModule,
    FieldsetModule,
    SplitButtonModule,
    TableModule,
    TagModule,
    ToolbarModule
  ]
})
export class PrimeNgModule { }
